import React from 'react';
import "./BotOverview.css";

class BotOverview extends React.Component {
    constructor(props){
        super(props);
        this.status = (props.status == "True") ? ("green_circle.png") : ("red_circle.png")
    }
    
    render() {
        return  (
            <>
                <div>
                    <img class="logo" src={this.props.img} alt="logo"/>
                </div>
                <h1>
                    {this.props.name}
                    <img src={this.status} alt="status" class="status"/>
                </h1>
            </>
        );
    }
}

export default BotOverview;