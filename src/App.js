import './App.css';
import BotOverview from './components/BotOverview'

function App() {
  return (
    <div className="App">
      <BotOverview name="bot_444" img="/444nuits.jpg" status="True"/>
    </div>
  );
}

export default App;
